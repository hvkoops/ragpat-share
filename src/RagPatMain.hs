{-# OPTIONS_GHC -Wall #-}
-- rtcm <- readRTC "compendium_post_vers_11.tsv"
-- rags <- ragPatDirRags "/home/hvkoops/repos/ragpat/out" rtcm
module Main where

import ZMidi.Score
import ZMidi.IO.Common                ( mapDirInDir, mapDir_, 
                                        readQMidiScoreSafe, mapDir )

-- other libraries
import System.Console.ParseArgs
import Control.Monad                  ( void, liftM )
import Ragtime.Compendium.Parser      ( readRTC, RTC (..) )
import Ragtime.Compendium.MatchFile   ( readRTCMidis, matchAll, 
                                        copyRTCMidi, printMatch )
import Ragtime.Pattern               
import Ragtime.PatternIO
import Ragtime.LongHigLee 
import Ragtime.Types     
import Ragtime.Statistics

import Data.Binary

data RagArgs = Mode| MidiDir | RTCFile | MidiFile | OutDir 
                 deriving (Eq, Ord, Show)


-- default options
-- TODO create CLI options for this
shortestNote :: ShortestNote
shortestNote = Sixteenth

opts :: QOpts
opts = QOpts shortestNote 0.02
--opts = QOpts shortestNote 0.1
                 
myArgs :: [Arg RagArgs]
myArgs = [ 
          Arg { argIndex = Mode,
                 argAbbr  = Just 'm',
                 argName  = Just "mode",
                 argData  = argDataRequired "rtc|stat|subdiv|mkrtc|lhl" 
                                                          ArgtypeString,
                 argDesc  = "Mode of operation"
               }
         , Arg { argIndex = MidiDir,
                 argAbbr  = Just 'd',
                 argName  = Just "dir",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Base directory containing the MIDI files"
               }
         , Arg { argIndex = RTCFile,
                 argAbbr  = Just 'c',
                 argName  = Just "rtc",
                 argData  = argDataOptional "filpath" ArgtypeString,
                 argDesc  = "The file containing the ragtime compendium"
               }
         , Arg { argIndex = MidiFile,
                 argAbbr  = Just 'f',
                 argName  = Just "file",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Do some tests on a single file"
               }
         , Arg { argIndex = OutDir,
                 argAbbr  = Just 'o',
                 argName  = Just "out-dir",
                 argData  = argDataOptional "filepath" ArgtypeString,
                 argDesc  = "Output Directory"
               }
         ] 

-- main for profiling
main :: IO ()
main = do rtcm <- readRTC "compendium_post_vers_11.tsv"
          ragPatDirLHL "out/" rtcm 
          return ()

---- main :: IO ()
--main = do arg <- parseArgsIO ArgsComplete myArgs
--          case (getRequiredArg arg Mode, fileOrDir arg) of
--           -- calculate some statistics for exploring the data
--           ("stat"  , Left f ) -> showFileStats f
--           ("stat"  , Right d) -> getCompendium arg >>= showDirStats d
         
--           -- -- Longuet-Higgins & Lee stuff
--           -- ("lhl"   , Left f ) -> doLHL (QOpts shortestNote 0.02) f
--           -- ("lhl"   , Right d) -> void . mapDirInDir 
--           --                 (mapDir_ (doLHL (QOpts shortestNote 0.02))) $ d
         
--           -- Creating a sub corpus
--           --("mkrtc" , Left  _) -> usageError arg 
--           --"building a sub-corpus requires a directory"
--           --("mkrtc" , Right d) -> getCompendium arg >>= createSubCorpus (getRequiredArg arg OutDir) d 
         
--           -- Use this option to generate the LHL patterns from the compendium
--           ("rtc"   , Right d) -> getCompendium arg >>= ragShowRags d
--           ("rtc"   , Left f ) -> printFilePatMat opts f
--           (m       , _      ) -> usageError arg ("invalid mode: " ++ m )



--main :: IO ()
--main = do arg <- parseArgsIO ArgsComplete myArgs
--          case (getRequiredArg arg Mode, fileOrDir arg) of
--            -- calculate some statistics for exploring the data
--            ("stat"  , Left f ) -> showFileStats f
--            ("stat"  , Right d) -> getCompendium arg >>= showDirStats d
            
--            -- Longuet-Higgins & Lee stuff
--            --("lhl"   , Left f ) -> doLHL (QOpts shortestNote 0.02) f
--            --("lhl"   , Right d) -> void . mapDirInDir (mapDir_ (doLHL (QOpts shortestNote 0.02))) $ d
            
--            -- Creating a sub corpus
--            ("mkrtc" , Left  _) -> usageError arg "building a sub-corpus requires a directory"
--            ("mkrtc" , Right d) -> getCompendium arg >>= createSubCorpus (getRequiredArg arg OutDir) d 
            
--            -- Use this option to regenerate the ISMIR 2013 results
--            ("rtc"   , Right d) -> getCompendium arg >>= ragPatDir d
--            ("rtc"   , Left f ) -> printFilePatMat opts f
--            (m       , _      ) -> usageError arg ("invalid mode: " ++ m )

--getRags :: IO [Rag]
--getRags = do arg <- parseArgsIO ArgsComplete myArgs
--             case (getRequiredArg arg Mode, fileOrDir arg) of
--          -- Use this option to create Rag types from the compendium
--               ("rag"   , Right d) -> getCompendium arg >>= ragPatDirRags d
--               -- ("rag"   , Left f ) -> ragPatDirRags opts f
   
--               (m       , _      ) -> usageError arg ("invalid mode: " ++ m )

-- | Checks for either a directory or file argument, returns them in an Either
-- or throws an error otherwise
fileOrDir :: Args RagArgs -> Either FilePath FilePath
fileOrDir arg = case (getArg arg MidiFile, getArg arg MidiDir) of
   (Just _, Just _) -> usageError arg "found both a directory and file"
   (Just f, _     ) -> Left f
   (_     , Just d) -> Right d
   (_     , _     ) -> usageError arg "No directory or file specified"
          
getCompendium :: Args RagArgs -> IO [RTC]
getCompendium arg = case getArg arg RTCFile of
                      Just c  -> readRTC c
                      Nothing -> usageError arg "no compendium specified"

-- | stuff to do with the rtc flag                      
ragPatDir :: FilePath -> [RTC] -> IO ()
ragPatDir d c = void . mapDirInDir (mapDir_ (printPatCount opts c)) $ d 

ragPatDirRags :: FilePath -> [RTC] -> IO Rags
ragPatDirRags d c = liftM concat $ mapDirInDir (mapDir $ getRag opts c d) $ d where

ragShowRags :: FilePath -> [RTC] -> IO ()
ragShowRags f c = void . mapDirInDir (mapDir_ (showRag opts c f)) $ f

ragPatDirLHL :: FilePath -> [RTC] -> IO ()
ragPatDirLHL f c = void . mapDirInDir (mapDir_ (printPatCountLHL opts c)) $ f

-- ragPatDirTimeSig :: FilePath -> [RTC] -> IO ()
-- ragPatDirTimeSig f c = void . mapDirInDir (mapDir_ (printTimeSig opts c)) $ f

-- | creates a subcorpus
createSubCorpus :: FilePath -> FilePath -> [RTC] -> IO ()
createSubCorpus out dir c = do m <- readRTCMidis opts dir
                               void $ matchAll (copyRTCMidi out) m c

-- | Print some stats
showFileStats :: FilePath -> IO ()
showFileStats fp = do mf <- readQMidiScoreSafe opts fp
                      case mf of
                        Left  err -> putStrLn (fp ++ '\t' : show err)
                        Right mid -> putStrLn . showMidiScore . qMidiScore $ mid

showDirStats :: FilePath -> [RTC] -> IO ()
showDirStats dir c = do m <- readRTCMidis opts dir
                        void $ matchAll (putStrLn . printMatch) m c

-- create binary file from parsed data
-- we need to have a Binary instance for Rags somehow
-- either derive: perhaps by generics deriving? 
-- or define ouselves: a lot of work?
parsedRags :: FilePath
parsedRags = "parsedrags.bin"

encodeRags :: FilePath -> Rags -> IO ()
encodeRags f r = encodeFile f (r :: Rags)