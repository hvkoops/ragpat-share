{-# OPTIONS_GHC -Wall                #-}
module Ragtime.Compendium.MatchFile ( -- | * Retrieving Meta data (after creating a matched corpus)
                   getRTCMeta 
                   -- | * Creating a matched corpus
                   -- | ** Reading
                 , readRTCMidis
                 , readRTCMidiPath
                   -- | ** Printing & Copying
                 , printMatch 
                 , copyRTCMidi
                   -- | ** Matching
                 , match
                 , matchAll
                 ) where

import Data.Tuple        ( swap )
import Data.List         ( stripPrefix, sortBy, groupBy, intercalate
                         , delete )
import Data.Text         ( unpack, pack, strip, breakOn )
import qualified Data.Text as T ( filter)
import Data.Char         ( toLower )
import Data.Maybe        ( catMaybes, isJust, fromJust )
import Data.Function     ( on )

import Control.Arrow     ( first, second )
import Control.Monad     ( when )


import ZMidi.IO.Common   ( mapDir, mapDirInDir, warning, readQMidiScoreSafe )
import ZMidi.Score       ( MidiScore (..), TimeSig (..), Timed (..)
                         , GridUnit, nrOfNotes, QMidiScore (..), QOpts
                         , ShortestNote (..) , canBeQuantisedAt )

import Ragtime.Types
import Ragtime.Compendium.Parser
import Ragtime.Pattern   ( getPercTripGridOnsets )
                         
import System.Directory  ( copyFile, createDirectoryIfMissing, doesFileExist )
import System.FilePath   ( (</>), (<.>), splitDirectories, takeFileName
                         , isPathSeparator, takeDirectory, makeValid) -- , dropExtension  )
import Control.Monad.Trans.State.Strict ( evalStateT, StateT (..), get, modify )
import Control.Monad.IO.Class    ( liftIO )
 
--------------------------------------------------------------------------------
-- selecting Midi files
--------------------------------------------------------------------------------

selectRTCMidi :: (RTC, Maybe RTCMidi) -> Bool
selectRTCMidi (rtc, mm) = 
  case mm of
    Nothing -> False
    Just m  ->    isValidYear      (year rtc)
               && isValidTimeSig   (rtcTimeSig m)
               -- && validGrid m 
               && isValidDeviation (rtcGridUnit m) (avgDeviation m)
 
-- | Is the 'TimeSig'natur a meter we can use for analysis?
isValidTimeSig :: [Timed TimeSig] -> Bool
-- there should be one valid time signature
isValidTimeSig [Timed ons ts] = case ons of 
                                  -- it should start at position 0 
                                  0 ->  case ts of 
                                          -- and should be 2/2 2/4 4/4
                                          (TimeSig 4 4 _ _) -> True 
                                          (TimeSig 2 4 _ _) -> True
                                          (TimeSig 2 2 _ _) -> True
                                          _                 -> False 
                                  _ -> False
isValidTimeSig _              = False
 
-- TODO: solve differently
-- | Is the quantisation deviation small enough for analysis?
isValidDeviation :: GridUnit -> Float -> Bool
isValidDeviation gu d = (d / fromIntegral gu) < 0.02
--------------------------------------------------------------------------------
-- A Datatype for matching midifiles to the ragtime compendium
--------------------------------------------------------------------------------
                         
-- -- | Representing a Midi filepath 
-- data RTCMidi = RTCMidi { baseDir      :: FilePath
--                        , folder       :: RTCFolder 
--                        , fileName     :: String
--                        -- some properties of the midifile 
--                        -- (We don't store the complete midi files to save space)
--                        , nrOfVoices   :: Int
--                        , rtcTimeSig   :: [Timed TimeSig]
--                        , tripletPerc  :: Double
--                        , avgDeviation :: Float
--                        , rtcGridUnit  :: GridUnit
--                        } deriving (Show, Eq)
 
-- compare the midi file based on statistics that might reflect the quality 
-- of the data: a) does it has a meter, b) how many different note lengths
-- do the files have (less is better = smaller), c) how many tracks do the files
-- have
compareRTCMidi :: RTCMidi -> RTCMidi -> Ordering
compareRTCMidi a b =  case compare (avgDeviation a) (avgDeviation b) of
                        EQ  -> compare (nrOfVoices a) (nrOfVoices b) 
                        x   -> x

 -- | Prints a match between a compendium entry and a midifile
printMatch :: (RTC, Maybe RTCMidi) -> String
printMatch (r,m) = let l  = [ show (rtcid r)   , show (title r)
                            , show (folders r) , show (year  r)
                            , approxYear (year r) ]
                       l' = case m of
                              Just p  -> l ++ [ show (selectRTCMidi (r,m))
                                              , show (nrOfVoices    p)
                                              , show (rtcTimeSig    p)
                                              , show (tripletPerc   p)
                                              , show (avgDeviation  p)
                                              , show (rtcGridUnit   p)
                                              , show (avgDeviation  p / 
                                                     (fromIntegral . rtcGridUnit $ p))
                                              , show ((avgDeviation p / 
                                                     (fromIntegral . rtcGridUnit $ p)) <= 0.1)
                                              , show (fileName      p) ]
                              Nothing -> l ++ ["n/a\tn/a\tn/a\tn/a\tn/a\tn/a\tn/a\tn/a\tn/a\tn/a\tn/a\tno matching file found"]
                   in intercalate "\t" l'

--------------------------------------------------------------------------------
-- IO stuff
--------------------------------------------------------------------------------

-- copies a matched midi file if it has a matching compendium entry
copyRTCMidi :: FilePath -> (RTC, Maybe RTCMidi) -> IO ()
copyRTCMidi newBase m@(rtc, mfrom) 
  | selectRTCMidi m = 
      do let from = fromJust mfrom
             to = toPath $ from { baseDir  = newBase
                                , fileName = makeValid ((unpack . strip . title $ rtc) <.> "mid")}
             fr = toPath from
         createDirectoryIfMissing True . takeDirectory $ to
         e <- doesFileExist fr
         if e then do copyFile fr to
                      putStrLn ("created\t" ++ printMatch m ++ "\t"++ to) 
              else putStrLn ("N.B. File does not exist: " ++ fr)
  -- Here we select if a match between a compendium entry and a midifile
  | otherwise = putStrLn ("ignored\t" ++ printMatch m)
  
 
-- | returns the filepath of the RTCMidi
toPath :: RTCMidi -> FilePath
toPath rtcf = case lookup (folder rtcf) folderMap of
  Just f  -> baseDir rtcf </> f </> fileName rtcf
  Nothing -> error ("folder not found" ++ show rtcf) 

  
-- | Collects (meta-)information about a MIDI file
readRTCMidis :: QOpts -> FilePath -> IO [RTCMidi]
readRTCMidis qo d =   mapDirInDir (mapDir (readRTCMidiPath qo d)) d 
                  >>= return . catMaybes . concat

-- reads a 'RTCMidi' given a base directory and a path to the file
readRTCMidiPath :: QOpts -> FilePath -> FilePath -> IO (Maybe RTCMidi)
readRTCMidiPath qo bd fp = -- Path conversions
  case stripPrefix (bd </> "") fp of
    Nothing -> error ("basedir and filepath do not match " ++ bd ++" /=  " ++ fp)
    Just f  -> -- get the name of the folder in the base directory
               let s = head . dropWhile (isPathSeparator . head) . splitDirectories $ f 
               in  case lookup s folderMapSwap of 
                     Nothing   -> error ("folder not found" ++ show s ++ " in " ++ fp)
                     Just rtcf -> readRTCMidi qo bd rtcf fp
               
readRTCMidi :: QOpts -> FilePath -> RTCFolder -> FilePath -> IO (Maybe RTCMidi)
readRTCMidi qo bd rtcf fp = 
  do m <- readQMidiScoreSafe qo fp 
     case m of
       Left  e -> warning fp e >> return Nothing -- ignore erroneous files
       -- Right x -> let r  = toRTCMidi x in  r `seq` (return . Just $ r) where
       Right x -> return . Just $! toRTCMidi x where
          -- | Strictly extract information needed for the selection of Midifiles from
          -- a 'MidiScore'.
          -- TODO adapt to new quantisation interface etc.
          toRTCMidi :: QMidiScore -> RTCMidi
          toRTCMidi qm@(QMidiScore ms _ gu d) = 
            let n          = length . getVoices $ ms  
                t          = getTimeSig ms
                p          = getPercTripGridOnsets qm
                k          = fromIntegral . nrOfNotes $ ms
                r          = RTCMidi bd rtcf (takeFileName fp) n t p (fromIntegral d / k) gu
            in n `seq` t `seq` p `seq` p `seq` d `seq` gu `seq` k `seq` r

--------------------------------------------------------------------------------
-- Matching Filenames
--------------------------------------------------------------------------------

-- State to keep track of the unmatched compendium entries
type MidiSt = ([RTCMidi],[(RTCFolder, [RTCMidi])])

-- | Applies a function that takes 'RTC' entry and a matched 'RTCMidi' file and 
-- does some computation to a dataset of 'RTCMidi's and a compendium.
matchAll :: ((RTC, Maybe RTCMidi) -> IO a) -> [RTCMidi] -> [RTC] -> IO [a]
matchAll f ms rs = evalStateT (mapM g rs) (ms, groupRTCMidis ms) where
  
  -- Match a compendium entry to the MIDI data
  -- g :: RTC -> StateT MidiSt IO a
  g x = match x >>= liftIO . f 

  -- | groups the RTCMidi's by their folder for easy matching
  groupRTCMidis :: [RTCMidi] -> [(RTCFolder, [RTCMidi])]
  groupRTCMidis m = let grp = groupBy ((==) `on` folder) m
                        ixs = map (folder . head) grp
                     in zip ixs grp

-- | matches 'RTCMidi's to an 'RTC' meta data entry
match ::  RTC -> StateT MidiSt IO (RTC, Maybe RTCMidi)
match r = do (ms, grp) <- get 
             let m = case folders r of
                       [] -> doMatch ms -- match against the complete corpus
                       f  -> case getSubSet f grp of -- or against a subdir
                               [] -> doMatch ms      -- if it's not empty
                               s  -> doMatch s
             -- delete the match from the corpus when it is a good match
             when (isJust . snd $ m) (modify (deleteMidi (fromJust . snd $ m)))
             return m where

  -- | Deletes an 'RTCMidi' from the 'MidiSt', this ensures we cannot match
  -- the same 'RTCMidi' twice.
  deleteMidi :: RTCMidi -> MidiSt -> MidiSt
  deleteMidi rtc (l, tab) = (delete rtc l, map (second (delete rtc)) tab)
               
  -- | returns a match      
  doMatch :: [RTCMidi] -> ((RTC, Maybe RTCMidi))
  doMatch subs = -- filter the matches
                 case filter snd . map (matchFN r) $ subs of
                 -- if there are no matches
                    []  -> (r, Nothing)
                 -- else resort the top of the list with a zero distance based 
                 -- on midi file statistics and pick the top file
                    top -> second Just . head . sortBy (compareRTCMidi `on` snd) 
                                       . map fst $ top

  -- | Returns a subset of the compendium based on pre-annotated folders
  getSubSet :: [RTCFolder] -> [(RTCFolder, [RTCMidi])] -> [RTCMidi]
  getSubSet fs = concat . map snd . filter ((flip elem) fs . fst) 
  
-- | Matches a filename 
matchFN :: RTC -> RTCMidi -> ((RTC, RTCMidi), Bool)
matchFN rtc mid = ( (rtc, mid)
                  , caseInsStrMatch (preProcRTC rtc) (preProcRTCMidi mid))
  where -- preprocessing
    preProcRTCMidi :: RTCMidi -> String
    preProcRTCMidi = unpack . T.filter wanted . fst . breakOn (pack " - ") 
                   . pack  
                   -- . dropExtension  -- this does seem to have a useful effect,
                                       -- but I don't understand the problem yet
                   . fileName 

    preProcRTC :: RTC -> String
    preProcRTC = unpack . T.filter wanted . title
    
    -- | strip and ignore special characters from the matched strings
    wanted :: Char -> Bool
    wanted c = not (c `elem` ",.\'\"-_ \t?!()[]`{}&~" )
               
    caseInsStrMatch :: String -> String -> Bool
    {-# INLINE caseInsStrMatch #-}
    caseInsStrMatch [] []         = True
    caseInsStrMatch [] _          = False
    caseInsStrMatch _  []         = False
    caseInsStrMatch (x:xs) (y:ys) = x `caseInsEQ` y && caseInsStrMatch xs ys
       
       where -- case insensitive matching
             caseInsEQ :: Char -> Char -> Bool
             caseInsEQ a b = toLower a == toLower b

--------------------------------------------------------------------------------
-- Mapping from RTC Folders to sub directories
--------------------------------------------------------------------------------
                        
  
folderMap :: [(RTCFolder, String)]              
folderMap =
  [(Cowles           , "Cowles"                                           )
  ,(MacDonald        , "MacDonald"                                        )
  ,(PittPayne        , "Pitt-Payne"                                       )
  ,(Reublin          , "Reublin"                                          )
  ,(Watanabe         , "Watanabe"                                         )
  ,(Wilson           , "Wilson"                                           )
  ,(MiscUnks         , "Misc Sequencers, unks"                            )
  ,(MiscSZ           , "Misc Sequencers S-Z excl unks"                    )
  ,(MiscMR           , "Misc Sequencers M-R"                              )
  ,(MiscFL           , "Misc Sequencers F-L"                              )
  ,(MiscAE           , "Misc Sequencers  A-E"                             )
  ,(AddedUniques     , "MIDIs added - Uniques"                            )
  ,(AddedHimpsl      , "MIDIs added - Himpsl uniques"                     )
  ,(AddedOther       , "MIDIs added - Have other renditions"              )
  ,(Mezjuev          , "Mezjuev"                                          )
  ,(Keller           , "Keller"                                           )
  ,(Hiawatha         , "Hiawatha & Radna"                                 )
  ,(Mathew           , "ex Mathew"                                        )
  ,(ElectriClef      , "ElectriClef (Witherwax)"                          )
  ,(Edwards          , "Edwards"                                          )
  ,(Decker           , "Decker, Kermit, Laura, Morgan, MSN, Midibiz & bTd")
  ,(CookieJar        , "Cookie Jar, Melody Lane & Crider"                 )
  ,(BokerTov         , "BokerTov, Bowie & Brunk"                          )
  ,(Crausaz          , "Crausaz & Orton"                                  )
  ,(Intartaglia      , "Intartaglia & ex Intartaglia"                     )
  ,(Treemonisha      , "Treemonisha (Davis & Wilson)"                     )
  ,(Trachtman        , "Trachtman"                                        )
  ,(TrachtmanRolls   , "Trachtman rolls"                                  )
  ,(Pianocorder      , "Pianocorder, PG Music, PianoDisc & Live (need to check Pianocorder titles)" )
  ,(PianocorderCheck , "Pianocorder to check"                             )
  ,(Perry            , "Perry, incl ex Perry"                             )
  ,(OldWeb           , "Old Web & PRT"                                    )
  ,(ODell            , "O'Dell"                                           )
  ,(Summers          , "Summers"                                          )
  ,(Smythe           , "Smythe"                                           )
  ,(Roache           , "Roache"                                           )
  ,(Ranalli          , "Ranalli"                                          )
  ,(Schwartz         , "Schwartz"                                         )
  ,(OldWeb           , "Old Web & PRT"                                    )
  ]
 
folderMapSwap :: [(String, RTCFolder)]
folderMapSwap = map swap folderMap
        