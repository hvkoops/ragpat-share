{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Ragtime.LongHigLee where
-- Summary: implements findings from H.C. Longuet-Higgins and C.S. Lee. 
-- In: "The rhythmic interpretation of monophonic music". 
-- Music Perception, 1:424–441, 1984.

-- see: http://scholar.google.nl/scholar?cluster=
  -- 14063899284112795236&hl=nl&as_sdt=0,5
-- Note (from Fitch, W. Tecumseh, and Andrew J. Rosenfeld. "Perception and
-- production of syncopated rhythms." (2007): 43-58.):
-- A Longuet-Higgins and Lee (1984) rhythm tree with an example rhythm and  
-- corresponding event weights. The rhythm has a moderate syncopation index 
-- of 5, and is indicated in two equivalent ways in standard musical notation and via Xs 
-- in the “box notation” above. The syncopation index value is calculated as follows 
-- (following Longuet-Hig- gins and Lee; see Appendix). Each “box” or event in the 
-- rhythmic tree has a pre-specified weight; the greatest weight (0) is assigned to 
-- the first event. The next strongest (−1) is on the “three,” the “two” and “four” 
-- are assigned weights of −2, and each of the “ands” (the off-beat eighth notes) 
-- are given a weight of −3. Sixteenth notes receive weight − 4, and so on. 
-- Syncopations occur when a rest (or tied note) “R” is pre- ceded by a sounded 
-- note “N” of lower weight. The difference in weights (R−N) is the syncopation 
-- value for each such pair. When a rhythm is pro- duced continuously, the values
-- wrap. In this example, the unsounded first rest, of weight 0, is preceded by a 
-- sounded eighth note of value −3, giving the first note a syncopation value of 
-- 0 minus −3 or 3. The next sounded note, −3, precedes a rest of value −2, giving 
-- a syncopation value of −2 minus −3, or 1. The same applies to the rest on the 
-- final beat. Thus the total syncopation value for this rhythm is (−2 − −3) + 
-- (−2 − −3) + (0 − − 3) = 5. See text and appendix for further details. 

import ZMidi.Score          ( QMidiScore (..), MidiScore (..), ShortestNote (..)
                            , TimeSig (..), getEvent, QOpts (..), qbins, toQBins
                            , Voice, onset, isNoteEvent, toOnsets, Time (..) )
import ZMidi.IO.Common            ( readQMidiScoreSafe, warning )
import ZMidi.Skyline.MelFind      ( mergeTracks, findMelodyQuant )
import IMA.InnerMetricalAnalysis hiding           ( Time(..) )
import qualified IMA.InnerMetricalAnalysis as IMA ( Time(..) )
import Data.List.Split
import Control.Monad        ( when )
import System.FilePath      ( (</>), takeFileName )
import Data.List            ( nubBy, zip4, zip5 )
import Data.Function        ( on )
import Data.List
import Data.Ord (comparing)

import Ragtime.Compendium.Parser
import Ragtime.Types

import Control.Monad
import ZMidi.IO.Common       ( mapDirInDir, mapDir_, readQMidiScoreSafe )

------------------------------------------------------------------------------
-- Longuet-Higgins & Lee
------------------------------------------------------------------------------
patternToText :: [BarPattern] -> [[Int]]
patternToText [] = []
patternToText (p:ps) = (:) (barPatternToText p) (patternToText ps) where

  barPatternToText :: BarPattern -> [Int]
  barPatternToText [] = []
  barPatternToText (X:s) = (:) (0) (barPatternToText s)
  barPatternToText (O:s) = (:) (0) (barPatternToText s)
  barPatternToText (I:s) = (:) (1) (barPatternToText s)

-- Example values from paper:
-- example values of a 4/4 bar with eighth notes
exvals8 :: [LHLVal]
exvals8 = cfg4 16
exvals8n :: Int -> [LHLTree]
exvals8n n = replicate n exvals8
expat8 :: Pattern
expat8 = [O, I, O, I, I, I, O, I]
expat16 :: BarPattern
expat16 = [O, O, I, O, O, O, I, O, I, O, I, O, O, O, I, O]
expat4 :: BarPattern
expat4 = [O, I, O, I]
expat4n :: Int -> [BarPattern]
expat4n n = replicate n expat4
expat8n :: Int -> [BarPattern]
expat8n n = replicate n expat8

-- lhl computes the LHL-syncopation of a [Pattern]
lhl :: [BarPattern] -> TimeSig -> ShortestNote -> [Syncopation] 
lhl p t@(TimeSig 2 4 _ _) sn = lhlSong p (patternLHLValues t sn)
lhl p t@(TimeSig 4 4 _ _) sn = lhlSong p (patternLHLValues t sn)
lhl _ _ _ = []

-- lhlMean computes the mean LHL-syncopation of a piece
lhlMean :: Fractional t => [BarPattern] -> TimeSig -> ShortestNote -> [t] 
lhlMean p t@(TimeSig 2 4 _ _) sn = getLHLMean $ 
                                      lhlSong p (patternLHLValues t sn)
lhlMean p t@(TimeSig 4 4 _ _) sn = getLHLMean $ 
                                      lhlSong p (patternLHLValues t sn)
lhlMean _ _ _ = []

-- convert the lhl to a list of RTCID, YEAR, LHL, [ONSETS]
lhlTupleLines :: Int -> Int -> [(Syncopation, BarPattern)] -> 
                                  [(Int, Int, Syncopation, [Int])]
lhlTupleLines i yr a = zip4 (repeat i) (repeat yr) 
                        ((map$fst) a) (map ptrnToInt $ (map$snd) a) 

-- convert the lhl to a list of RTCID, COMPOSER, YEAR, LHL, [ONSETS]
lhlTupleLinesICYS :: Int -> String -> Int -> [RagBarR] -> 
                                  [(Int, String, Int, Syncopation, [Int])]
lhlTupleLinesICYS i c yr a = zip5 (repeat i) (repeat . fixComposer $ c)  (repeat yr) 
                        (map barsync a) (map (ptrnToInt.pattern) a) 

--remove commas and slashes
fixComposer :: String -> String
fixComposer = filter (/=',') . filter (/='\"')

-- convert the lhl to a list of YEAR, [(BARLEN, [ONSETS])]
lhlTupleNum3 :: Int -> [(Syncopation, BarPattern)] -> 
                                    [(Int, Syncopation, (Int, [Int]))]
lhlTupleNum3 yr a = zip3 (repeat yr) ((map$fst) a) 
                                (map ptrnToIntLength $ (map$snd) a) 

-- convert the lhl to a list of RTCID, YEAR, LHL, [ONSETS]
lhlTupleNum2 :: Int -> [(Syncopation, BarPattern)] -> 
                                  [(Int, Syncopation, (Int, [Int]))]
lhlTupleNum2 yr a = zip3 (repeat yr) ((map$fst) a) 
                                (map ptrnToIntLength $ (map$snd) a) 

-- rescale bar to double length
downsampleDouble :: LHLPattern -> LHLPattern
downsampleDouble []       = []
downsampleDouble ((s,b):bs) = (s,(intercalateSilence b)):(downsampleDouble bs)

-- rescale bar to double length
downsampleDoubleR :: [RagBarR] -> [RagBarR]
downsampleDoubleR []         = []
downsampleDoubleR (RagBarR bs os p:ps) = 
            (RagBarR bs os (intercalateSilence p)):(downsampleDoubleR ps)

-- we only care about the tactus: 
lhlTupleTactusR :: [BarPattern] -> Tactus -> ShortestNote -> [RagBarR]
lhlTupleTactusR p 4 sn = lhlSongTupleR p 
                              (patternLHLValues (TimeSig 4 4 0 0) sn)
lhlTupleTactusR p 2 sn = downsampleDoubleR $ lhlSongTupleR p 
                              (patternLHLValues (TimeSig 2 4 0 0) sn)
lhlTupleTactusR _                   _ _  = []

splitByMask :: Ord a => [(a, b)] -> [(a, [b])]
splitByMask = map (\l -> (fst . head $ l, map snd l)) . 
                groupBy ((==) `on` fst) . sortBy (comparing fst)

-- Chunks with O endings in case of length divisor with remainder
chunksOfExt :: Int -> [Onset] -> [[Onset]]
chunksOfExt n p = chunksOf n $ p ++ (take (n-(mod (length p) n)) $ repeat O)

-- lhlTupleCy computes the LHL-syncopation of a cyclic [Pattern], 
-- returns [(Syncopation, [Pattern])]
-- disregards tied patterns over barlines, we dont use this anymore
lhlTupleCy :: [BarPattern] -> TimeSig -> ShortestNote -> LHLPattern
lhlTupleCy p t@(TimeSig 2 4 _ _) sn = lhlSongTupleCy p (patternLHLValues t sn)
lhlTupleCy p t@(TimeSig 4 4 _ _) sn = lhlSongTupleCy p (patternLHLValues t sn)
lhlTupleCy _ _ _ = []

ptrnToIntLength :: BarPattern -> (Int, [Int])
ptrnToIntLength a = (length a, map onsetToInt a)

ptrnToInt :: BarPattern -> [Int]
ptrnToInt = map onsetToInt

noBrackets :: [Char] -> [Char]
noBrackets a = filter ((/= ']')) $  filter ((/= '[')) $ a

onsetToInt :: Onset -> Int
onsetToInt O = 0
onsetToInt X = 0
onsetToInt I = 1

-- getLHLMean computes the mean LHL-syncopation of a piece
getLHLMean :: Fractional t => [Syncopation] -> [t]
getLHLMean a = [(((/) `on` fromIntegral) (sum $ map sync a) (length a))]
getLHLMean [] = [0]

-- lhl computes the LHL-syncopation of a [BarPattern]
lhlNoSum :: [BarPattern] -> TimeSig -> ShortestNote -> [[Syncopation]]
lhlNoSum p t@(TimeSig 2 4 _ _) sn = lhlSongNoSum p (patternLHLValues t sn)
lhlNoSum p t@(TimeSig 4 4 _ _) sn = lhlSongNoSum p (patternLHLValues t sn)
lhlNoSum _ _ _ = [[]]

-- create the binary LHL-value tree 
patternLHLValues :: TimeSig -> ShortestNote -> LHLTree
patternLHLValues (TimeSig n 4 _ _) sn = cfg4 ((qbins.toQBins $ sn)*n)
patternLHLValues (TimeSig n 2 _ _) sn = cfg4 ((qbins.toQBins $ sn)*(n*2)) 
patternLHLValues _ _ = []

-- cfg4 creates a value pattern for a 4/4 bar until a given depth
cfg4 :: Int -> LHLTree
cfg4 0 = []
cfg4 n = cfg4' [0] n 0  where

  cfg4' :: [Int] -> Int -> Int -> LHLTree
  cfg4' a n' d = case (((length a) - n') /= 0) of 
       False  -> map LHLVal $ concat [a]
       True   -> cfg4' (concat $ map (cfgrule (d+1)) a) n' (d+1) 
  -- cfgrule: simple cfg-style rewrite rule 
  -- to create the lhl value tree: [x]@d -> [x (d-1)]
  cfgrule :: Num t => t -> t -> [t]
  cfgrule td a = [a, -td]

-- Measure LHL-syncopation of multiple bars
lhlSongTupleR :: [BarPattern] -> LHLTree -> [RagBarR]
lhlSongTupleR [] _ = []
lhlSongTupleR o  v = lhlSongTupleAccR o v [] where

-- Measure LHL-syncopation of multiple cyclic bars
lhlSongTupleCy :: [BarPattern] -> LHLTree -> LHLPattern
lhlSongTupleCy [] _ = []
lhlSongTupleCy o  v = lhlSongTupleAccCy o v [] where

-- Measure LHL-syncopation of multiple bars
lhlSong :: [BarPattern] -> LHLTree -> [Syncopation]
lhlSong [] _ = []
lhlSong o  v = lhlSongAcc o v [] where

lhlSongTupleAccR :: [BarPattern] -> LHLTree -> [BarPattern] -> [RagBarR]
lhlSongTupleAccR []       _ s = [] 
lhlSongTupleAccR (o:[])   v s = 
  (:) (RagBarR bsyn syn ptrn) (lhlSongTupleAccR [] v s) where
        bsyn = lhlBar (o++[head o]) (v++[head v])
        syn  = lhlBarNoSum (o++[head o]) (v++[head v])
        ptrn = o++(take (div (length o) 2) (repeat O))
lhlSongTupleAccR (o:p:os) v s = 
  (:) (RagBarR bsyn syn ptrn) (lhlSongTupleAccR (p:os) v s) where
        bsyn = lhlBar (o++[head p]) (v++[head v]) 
        syn  = lhlBarNoSum (o++[head p]) (v++[head v])
        ptrn = o++(take (div (length p)2)p)

-- Measure LHL-syncopation of multiple cyclic bars with accumulator
lhlSongTupleAccCy :: [BarPattern] -> LHLTree -> [BarPattern] -> LHLPattern
lhlSongTupleAccCy []       _ s = [] 
lhlSongTupleAccCy (o:[])   v s = (:) ((lhlBar (o++[head o]) 
                              (v++[head v])),o) (lhlSongTupleAccCy [] v s) 
lhlSongTupleAccCy (o:os)   v s = (:) ((lhlBar (o++[head o]) 
                              (v++[head v])),o) (lhlSongTupleAccCy os v s)

-- Measure LHL-syncopation of multiple bars with accumulator
lhlSongAcc :: [BarPattern] -> LHLTree -> [Syncopation] -> [Syncopation]
lhlSongAcc []        _  s = s 
lhlSongAcc (o':[])   v' s = (:) (lhlBar (o'++[head o']) 
                                  (v'++[head v'])) (lhlSongAcc []     v' s) 
lhlSongAcc (o':p:os) v' s = (:) (lhlBar (o'++[head p ]) 
                                  (v'++[head v'])) (lhlSongAcc (p:os) v' s)

lhlSongNoSum :: [BarPattern] -> LHLTree -> [[Syncopation]]
lhlSongNoSum [] _ = []
lhlSongNoSum o  v = lhlSongAccNoSum o v [] where

-- Measure LHL-syncopation of multiple bars with accumulator
lhlSongAccNoSum :: [BarPattern] -> LHLTree -> [Syncopation] -> [[Syncopation]]
lhlSongAccNoSum []        _  s = [s] 
lhlSongAccNoSum (o':[])   v' s = (:) (lhlBarNoSum (o'++[head o']) 
                              (v'++[head v'])) (lhlSongAccNoSum []     v' s) 
lhlSongAccNoSum (o':p:os) v' s = (:) (lhlBarNoSum (o'++[head p ]) 
                              (v'++[head v'])) (lhlSongAccNoSum (p:os) v' s)

-- Sum the LHL-syncopation of one bar
lhlBar :: BarPattern -> LHLTree -> Syncopation
lhlBar []      valuetree = (Syncopation 0)
lhlBar pattern valuetree = sum $ lhlCheck fpattern fvaluetree [] where
                  depth      = lhlDephth valuetree pattern
                  fpattern   = (snd.unzip) $ 
                                filterSecond valuetree (>= depth) pattern
                  fvaluetree = (fst.unzip) $ 
                                filterSecond valuetree (>= depth) pattern

-- LHL-syncopations of one bar
lhlBarNoSum :: BarPattern -> LHLTree -> [Syncopation]
lhlBarNoSum pattern valuetree = lhlCheck fpattern fvaluetree [] where
                  depth      = lhlDephth valuetree pattern
                  fpattern   = (snd.unzip) $ 
                                filterSecond valuetree (>= depth) pattern
                  fvaluetree = (fst.unzip) $ 
                                filterSecond valuetree (>= depth) pattern

-- lhlDephth finds the maximum depth of the lhl tree where a note onset occurs
lhlDephth :: (Num a, Ord a) => [a] -> [Ragtime.Types.Onset] -> a
lhlDephth [] _  = 0
lhlDephth a  b  | (depthList == []) = 0
                | otherwise         = (nub $ sort a)!!(head depthList) where
                       depthList = elemIndices True $ 
                            (map (elem I) $ map snd $ splitByMask $ zip a b)

-- Measure the LHL-syncopation of one bar with accumulator
-- Syncopations occur when a rest (or tied note) “O” is preceded by 
--  a sounded note “I” of lower weight. 
-- The difference in weights (O−I) is the syncopation value for each such pair. 
lhlCheck :: BarPattern -> LHLTree -> LHLTree -> [Syncopation]
lhlCheck []       _       acc          = map Syncopation $ map lhlTreeVal acc
lhlCheck (I:O:xs) (o:p:op) acc | (o<p) = lhlCheck xs op ((p-o):acc)
lhlCheck (O:xs)   (_:op)   acc         = lhlCheck xs op (0:acc)
lhlCheck (X:xs)   (_:op)   acc         = lhlCheck xs op (0:acc)
lhlCheck (I:xs)   (_:op)   acc         = lhlCheck xs op (0:acc)

filterSecond :: [a] -> (a -> Bool) -> [b] -> [(a,b)]
filterSecond [] _ _ = []
filterSecond (x:xs) c (b:bx) | (c x)     = (x,b):(filterSecond xs c bx)
                             | otherwise = (filterSecond xs c bx)

-- create sublists of length n and fill with O's at the end if needed
fixPatterns :: [Onset] -> Int -> [Pattern]
fixPatterns p n = splitEvery n $ p ++ 
                    (take (n-((flip rem) n $ length $ p)) $ repeat O)

-- stretch a pattern one level higher
intercalateSilence :: [Ragtime.Types.Onset] -> [Ragtime.Types.Onset]
intercalateSilence = (flip (++) [O]).(intercalate [O]).(map (\a -> [a]))

