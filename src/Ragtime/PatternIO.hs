module Ragtime.PatternIO  where

import ZMidi.Score
import ZMidi.IO.Common            ( readQMidiScore, readQMidiScoreSafe, 
                                    warning )
import ZMidi.Skyline.MelFind      ( findMelodyQuant, getAccompQuant )
import ZMidi.IMA.GTInfo 
import Ragtime.Compendium.Parser  ( RTC (..), getRTCMeta, approxYear, 
                                    preciseYear, preciseYearInt, 
                                    rtcidInt, approxYearInt )

import Data.List                  ( intercalate )
import Control.Arrow              ( first, second )
import Ragtime.Evaluation

import Data.List.Split
import Data.List
import Ragtime.LongHigLee 
import Ragtime.Types
import Control.Monad
import System.FilePath.Posix
import Ragtime.Compendium.MatchFile
import Ragtime.Pattern

--------------------------------------------------------------------------------
-- Metafunctions for patternfinding for IO
--------------------------------------------------------------------------------

printPatCount :: QOpts -> [RTC] -> FilePath -> IO ()
printPatCount qo rtc f = 
  do ms <- readQMidiScore qo f 
     let ts  = getTimeSig . qMidiScore $ ms
         ts' = getEvent . head $ ts
         ps  = createOverlapSegs ts' . toPatterns $ ms
         mt  = getRTCMeta rtc f
         yr  = year mt
     putStrLn . intercalate "\t"  
              $ ( show (rtcid mt) : f 
                : show ts : approxYear yr 
                : map (show . matchRatio . matchPat ps . upScalePat ts') 
                  [ untied1, untied2, tied1, tied2 ]
                )

printPatCountLHL :: QOpts -> [RTC] -> FilePath -> IO ()
printPatCountLHL qo@(QOpts sn _) rtc f = 
 do ms <- readQMidiScore qo f 
    let ts           = getTimeSig . qMidiScore $ ms
        ts'          = getEvent . head $ ts
        mt           = getRTCMeta rtc f
        yr           = approxYearInt $ year mt
        comp         = show . composer $ mt
        p            = toPatterns ms
        tactus       = findTactus ms
        ptrn         = fixPatterns (concat p) (tactus * 4)        
        syncopation  = lhlTupleTactusR ptrn tactus sn
        songid       = rtcidInt (rtcid mt)
    case (syncopation /= []) of True  -> putStrLn $ intercalate "\n" $ map (removeBrackets . show) $ lhlTupleLinesICYS songid comp yr syncopation
                                False -> return ()

removeBrackets :: [Char] -> [Char]
removeBrackets = filter (/=')') . filter (/='(')

printPatCountLHLString :: QOpts -> [RTC] -> FilePath -> IO (String)
printPatCountLHLString qo@(QOpts sn _) rtc f = 
 do ms <- readQMidiScore qo f 
    let ts           = getTimeSig . qMidiScore $ ms
        ts'          = getEvent . head $ ts
        mt           = getRTCMeta rtc f
        yr           = approxYearInt $ year mt
        comp         = show . composer $ mt
        p            = toPatterns ms
        tactus       = findTactus ms
        ptrn         = fixPatterns (concat p) (tactus * 4)        
        syncopation  = lhlTupleTactusR ptrn tactus sn
        songid       = rtcidInt (rtcid mt)
    case (syncopation /= []) of True  -> return $ intercalate "\n" $ map show $ lhlTupleLinesICYS songid comp yr syncopation
                                False -> return ("")

-- Instead of making a String for CSV, put results in a new datatype
getRag :: QOpts -> [RTC] -> FilePath -> FilePath -> IO Rag
getRag qo@(QOpts sn _) rtc bd f = 
  do ms <- readQMidiScore qo f 
     let ts           = getTimeSig . qMidiScore $ ms
         ts'          = getEvent . head $ ts
         mt           = getRTCMeta rtc f
         p            = toPatterns ms
         tactus       = findTactus ms
         ptrn         = fixPatterns (concat p) (tactus * 4) 
         -- ptrn'        = downsampleTactus tactus ptrn
         mel          = toMelody ms ptrn
         rp           = lhlTupleTactusR ptrn tactus sn
     Just rm  <- readRTCMidiPath qo bd f
     ts `seq` ts' `seq` mt `seq` p `seq` tactus `seq` 
      ptrn `seq` mel `seq` rp `seq` rm  `seq` return (Rag mt rm rp mel)

printTactusInfo :: QOpts -> IO ()
printTactusInfo qo@(QOpts sn _) = do
     Just gtData <- maybeReadGT $ Just "/home/hvkoops/repos/ragpat/Ragtime_Sample_200/RagtimeTestGT_osx.csv"
     let fnames       = map gtFile gtData
         meters       = map gtMeters gtData
         -- nums         = map head $ (map.map) (tsNum.gtTimeSig) meters
         nums         = map head $ (map.map) (gtTimeSig) meters
         notes        = map gtNotes gtData     
         namesNums    = zip fnames nums
     mapM_ (doTactus qo) namesNums

-- printTimeSig :: QOpts -> [RTC] -> FilePath -> IO ()
-- printTimeSig qo@(QOpts sn _) rtc f = 
--   do ms <- readQMidiScore qo f 
--      let ts           = getTimeSig . qMidiScore $ ms
--          ts'          = getEvent . head $ ts
--          mt           = getRTCMeta rtc f
--          yr           = approxYearInt $ year mt
--          p            = take 4 $ toPatterns ms
--          tactus       = findTactus ms
--          ptrn         = fixPatterns (concat p) (tactus * 4)        
--          syncopation  = lhlTupleTactus ptrn ts' tactus sn
--          songid       = rtcidInt (rtcid mt)
--      putStrLn $ (show f) ++ "\t" ++ (show ts') ++ "\t" ++ (show p)

-- output csv lines for the SMT class
showRag :: QOpts -> [RTC] -> FilePath -> FilePath -> IO ()
showRag qo@(QOpts sn _) rid bd f = 
  do  rag <- getRag qo rid bd f 
      let i     = show $ rtcid.rtc $ rag 
          yr    = preciseYear.year.rtc $ rag
          rp    = map pattern $ rpatterns $ rag
          mp    = map mel $ mpatterns $ rag
          rmp   = zip rp mp
      putStrLn $ intercalate "\n" $ map (ragLine f i yr) rmp

-- create one line for csv file
ragLine :: (Show a, Show a1) => FilePath -> [Char] -> 
                                [Char] -> (a, a1) -> [Char]
ragLine f i y (r,m) = intercalate "," $ [i,(show f),y,(show r),(show m)]

doTactus :: QOpts -> (FilePath, TimeSig) -> IO ()
doTactus qo (f,i) = readQMidiScoreSafe qo f >>= either (warning f) (print . findTactus' i)

-- printMeterStats :: QOpts -> [RTC] -> FilePath -> IO ()
-- printMeterStats qo@(QOpts sn _) rtc f = 
--   do ms <- readQMidiScore qo f 
--      let ts           = getTimeSig . qMidiScore $ ms
--          ts'          = getEvent . head $ ts
--          ps           = createOverlapSegs ts' . toPatterns $ ms
--          mt           = getRTCMeta rtc f
--          yr           = approxYearInt $ year mt
--          p            = toPatterns ms
--          barLength    = (tsNum $ getEvent $ head $ getTimeSig $ qMidiScore ms)*(qbins.toQBins $ sn)
--          -- ptrn         = fixPatterns (chunksOf barLength $ concat p) (patternLHLValues ts' sn) []
--          ptrn         = fixPatterns (concat p) barLength
--          tactus       = findTactus ms
--          syncopation  = lhlTupleTactus ptrn ts' tactus sn
--          songid       = rtcidInt (rtcid mt)
--          num          = tsNum . getEvent . head $ ts
--      case (num /= tactus) of True  -> putStrLn $ intercalate "," $ ([show ts]) ++ ([show tactus]) ++ ([show f])
--                              False -> return ()    

-- | Match the patterns to one file
-- TODO replace FourtyEighth by Quantise parameter
-- TODO adapt to new Quantisation interface
printFilePatMat :: QOpts -> FilePath -> IO ()
printFilePatMat qo f =  
  do ms <-readQMidiScore qo f 
     let ts = getEvent . head . getTimeSig . qMidiScore $ ms
         ps = createOverlapSegs ts . toPatterns $ ms
     putStrLn . showPats $ ps
     putStrLn ("Time Signtaure: " ++ show ts)
     
     print (upScalePat ts untied1)
     print (upScalePat ts untied2)
     putStrLn ("untied1: " ++ (show . matchPat ps . upScalePat ts $ untied1))
     putStrLn ("untied2: " ++ (show . matchPat ps . upScalePat ts $ untied2))
     
     print (upScalePat ts tied1)
     print (upScalePat ts tied2)
     putStrLn ("tied1  : " ++ (show . matchPat ps . upScalePat ts $ tied1  ))
     putStrLn ("tied2  : " ++ (show . matchPat ps . upScalePat ts $ tied2  ))
     
     -- putStrLn ("tiedStr: " ++ (show . matchPat ps . upScalePat ts $ tiedStr))