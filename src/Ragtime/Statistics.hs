{-# OPTIONS_GHC -Wall                #-}
-- |
-- Module      :  Ragtime.Statistics
-- Copyright   :  (c) 2015, Utrecht University 
-- License     :  LGPL-3
--
-- Maintainer  :  H.V. Koops <h.v.koops@uu.nl>
-- Stability   :  experimental
-- Portability :  non-portable
--
-- Summary: perform statistics on (rhythmic) patterns in rags

-- Perhaps rename this to e.g. Rhythm Statistics ?
module Ragtime.Statistics where

import Ragtime.Types
import Data.List
import Ragtime.Compendium.Parser
import Control.Monad
import Control.Applicative
import Data.Functor
import Data.Maybe
import Data.Function
import Ragtime.LongHigLee
import Numeric (showIntAtBase)
import Data.Char (intToDigit, digitToInt)
import Data.Ord (comparing)
import Data.List.Split

-- a very distant year
future :: Int
future = 3000
allEras :: Era
allEras = (0,future)

-- Berlin eras
ragtimeEra :: Era
ragtimeEra = (0, 1920)
modernEra :: Era
modernEra  = (1920, future)
-- Jensen eras: 
earlyRagtime :: Era -- 439
earlyRagtime 	= (1897,1905)
popularRagtime :: Era -- 624
popularRagtime 	= (1906,1912)
advancedRagtime :: Era -- 398
advancedRagtime = (1913,1917) 
noveltyRagtime :: Era -- 336
noveltyRagtime  = (1918,1928)
strideRagtime :: Era -- 346
strideRagtime 	= (1918,1929)

-- some periods to find rags
decade :: Period
decade = 10
century :: Period
century = 100
millenium :: Period
millenium = 1000

-- create an era from a starting year with a period (e.g. decade)
createEra :: Period -> Year -> Era
createEra p y = (y, y+p)
-- create eras from a starting year until an end year with a period 
createEras :: Period -> Year -> Year -> [Era]
createEras p sy ey = [createEra p ry | ry <- [sy, (sy+p) .. ey]] 

-- return rags from the modern era (i.e. year > 1920)
getModernEra :: Rags -> Rags
getModernEra r = getEra r $ modernEra
-- return rags from the ragtime era (i.e. year < 1920)
getRagTimeEra :: Rags -> Rags
getRagTimeEra r = getEra r $ ragtimeEra
--return Rags between year>=n and year<=m
getEra :: Rags -> Era -> Rags
getEra r (n,m) = map ((!!) r) ind where
	ind = findIndices ((liftM2 (&&)) (<m) (>=n)) $ 
			map (preciseYearInt.year) $ map rtc r	

ragHasSyncopation :: Rag -> Bool
ragHasSyncopation r = (flip (>) 0).sum.(map barsync).rpatterns $ r

-- get bars with no syncopation (i.e. LHL = 0)
filterNoSyncBars :: [RagBarR] -> [RagBarR]
filterNoSyncBars b = filterBarsFSync (==0) $ b
-- get bars with syncopation (i.e. LHL > 0)
filterSyncBars :: [RagBarR] -> [RagBarR]
filterSyncBars b = filterBarsFSync (> 0) $ b
-- get bars with syncopation satisfying the predicate f
filterBarsFSync :: (Int -> Bool) -> [RagBarR] -> [RagBarR]
filterBarsFSync f b = map ((!!) b) ind where
	ind = findIndices f $ map (sync.barsync) $ b 
filterSyncRag :: Rag -> Rag
filterSyncRag (Rag r i p m) = Rag (r) (i) (filterSyncBars p) m
filterNoSyncRag :: Rag -> Rag
filterNoSyncRag (Rag r i p m) = Rag (r) (i) (filterSyncBars p) m

-- get bars with n syncopation
getBarsNSync :: Int -> Rag -> [BarPattern]
getBarsNSync n r = map ((!!) (map pattern $ rpatterns r)) ind where
	ind = findIndices (==n) $ 
			map (sync.barsync) $ rpatterns r

-- return the two tied patterns of a list in a tuple
getTiedPat :: BinOnsets -> TiedPatternsBoth
getTiedPat p = ((take leng $ drop u1 $ p),(take leng $ drop u2 $ p))   where
	u1  = fst (tiedPos $ closestPowerTwo (length p))
	u2  = snd (tiedPos $ closestPowerTwo (length p))
	leng = u2-u1

-- return the two untied patterns of a list in a tuple
getUntiedPat :: BinOnsets -> TiedPatternsBoth
getUntiedPat p = ((take leng $ drop u1 $ p),(take leng $ drop u2 $ p))  where
	u1  = fst (untiedPos $ closestPowerTwo (length p))
	u2  = snd (untiedPos $ closestPowerTwo (length p))
	leng = u2-u1

-- get the frequecies of tied patterns
tiedFreqs :: [BinOnsets] ->	TiedPatternsBoth
tiedFreqs   p = ((map ((flip (-) 1).length).group.sort.addZeroCounts.
									(map (binToDec.fst.getTiedPat)) $ p),
	             (map ((flip (-) 1).length).group.sort.addZeroCounts.
	             					(map (binToDec.snd.getTiedPat)) $ p))

-- get the frequecies of untied patterns
untiedFreqs :: [BinOnsets] -> UntiedPatternsBoth
untiedFreqs p = ((map ((flip (-) 1).length).group.sort.addZeroCounts.
									(map (binToDec.fst.getUntiedPat)) $ p),
	             (map ((flip (-) 1).length).group.sort.addZeroCounts.
	             					(map (binToDec.snd.getUntiedPat)) $ p))

getTied :: (TiedPatterns,UntiedPatterns) -> TiedPatterns
getTied = fst
getUntied :: (TiedPatterns,UntiedPatterns) -> UntiedPatterns
getUntied = snd

-- return the vertical sum of all pattern frequencies in all Rags
collapseFreqs :: [BinOnsets] -> BinOnsets
collapseFreqs p = (map sum) $ transpose p

-- Takes a list of Rags and returns the (Tied, Untied) pattern frequencies
getAllTiedUntiedFreqs :: Rags -> (TiedPatternsBoth,UntiedPatternsBoth)
getAllTiedUntiedFreqs r = ((t1,t2),(u1,u2)) where
		p  = (map.map.map) onsetToInt $ map ((map pattern).rpatterns) r
		t1 = collapseFreqs $ (map fst) $ map tiedFreqs p
		t2 = collapseFreqs $ (map snd) $ map tiedFreqs p
		u1 = collapseFreqs $ (map fst) $ map untiedFreqs p
		u2 = collapseFreqs $ (map snd) $ map untiedFreqs p

-- combine the first and second tied and untied
getCombinedTiedUntiedFreqs :: Rags -> (TiedPatterns,UntiedPatterns)
getCombinedTiedUntiedFreqs r = (t,u) where
									t = (map (\x -> (fst x+snd x))) $ 
									         (\y -> zip (fst y) (snd y)) $
											fst.getAllTiedUntiedFreqs $ r
									u = (map (\x -> (fst x+snd x))) $ 
											 (\y -> zip (fst y) (snd y)) $
											snd.getAllTiedUntiedFreqs $ r

normalizeList :: (Fractional b, Integral b1) => [b1] -> [b]
normalizeList l = map (flip ((/) `on` fromIntegral) sf) l where
				 	sf = sum l

-- make the minimum count 1 
addZeroCounts :: BinOnsets -> BinOnsets
addZeroCounts [] = []
addZeroCounts p =  p++([0..mx]) where
	mx = head.reverse.sort $ p

topNPatterns :: (Enum b, Fractional a, Integral b1, Num b, Ord a) =>
     				Int -> [b1] -> [(a, b)]
topNPatterns n p = take n $ reverse $  
						sortBy (comparing fst) (zip (normalizeList p) [0..])

-- return the untied positions of a bar of length n
untiedPos :: Int -> (Int, Int)
untiedPos n | (n `mod` 2) == 0 	= (0, n `div` 2)
			| otherwise 		= (-1, -1)
-- return the tied positions of a bar of length n
tiedPos :: Int -> (Int, Int)
tiedPos n 	| (n `mod` 2) == 0 	= ((n `div` 4), ((n `div` 4)+(n `div` 2)))
	 		| otherwise 		= (-1, -1)

isPowerTwo :: Integral a => a -> Bool
isPowerTwo n = elem n$map(2^)[1..n]

-- floor of the closest power of two
closestPowerTwo :: Integral a => a -> a
closestPowerTwo n = (head.(dropWhile (<= n)) $ map(2^)[1..n]) `div` 2

-- generate all binary patterns of a given length
binPatt :: Int -> [[Int]]
binPatt n = ((dropWhile (\y -> (length y)  <n)) . 
			  takeWhile (\x -> (length x) <=n)) $ 
			(`replicateM`[0,1])=<<[1..]

-- return the binary representation of a number with a min length
decToBin :: Int -> Int -> [Int]
decToBin l n = (take (l-(length s)) $ repeat 0)++s where 
	s = decToBin' n where
		decToBin' :: Int -> [Int]
		decToBin' m = (map digitToInt) $ showIntAtBase 2 intToDigit m ""

-- add a star to the 121 pattern
decToBinStar :: Int -> Int -> String
decToBinStar l 162 = show ((take (l-(length s)) $ repeat 0)++s) ++ "*" where 
	s = decToBin' 162 where
		decToBin' :: Int -> [Int]
		decToBin' m = (map digitToInt) $ showIntAtBase 2 intToDigit m ""
decToBinStar l n = show ((take (l-(length s)) $ repeat 0)++s) where 
	s = decToBin' n where
		decToBin' :: Int -> [Int]
		decToBin' m = (map digitToInt) $ showIntAtBase 2 intToDigit m ""

binToDec :: [Int] -> Int
binToDec b = fromJust $ foldM (\a d -> (a*2 +) <$> elemIndex d [0,1]) 0 b

-- bin the elements of list l in b bins and sum
binListSum :: Num b => Int -> [b] -> [b]
binListSum b l | (rem (length l) b > 0) = (map sum) $ (chunksOf (n+1)) l 
			   | otherwise 			    = (map sum) $ (chunksOf n) l where 
				   n = div (length l) b 

-- bin the elements of list l in b bins
binList :: Int -> [b] -> [[b]]
binList b l | (rem (length l) b > 0) = (chunksOf (n+1)) l 
		    | otherwise 			 = (chunksOf n) l where 
			   n = div (length l) b 

-- pretty printer for the top N list of tuples
ppTuples :: [(Double, Int)] -> IO ()
ppTuples l = mapM_ (putStrLn . f) $ l
  where f (a,b) = show a ++ "\t" ++ show (decToBin 8 b)

ppTwoTuples :: ((Double, Int),(Double, Int)) -> String
ppTwoTuples ((t,nt),(u,un)) = 	show t ++ "\t" ++ (decToBinStar 8 nt) ++ "\t"
								++ show u ++ "\t" ++ (decToBinStar 8 un) 
								++"\n"

-- pretty print the two top N lists of an era side-by-side
-- todo: add number of rags in era
ppTopN :: Int -> Era -> Rags -> IO ()
ppTopN n e r = 
	mapM_ putStr $	["Era:",(show e)," #rags: ",show $ length era,"\n",
			 			"Tied:","\t\t\t\t\t\t","Untied:","\n"] ++
			 			(map ppTwoTuples $ zip t u) 
	where
		era = getEra r e
		t = topNPatterns n $ getTied $ getCombinedTiedUntiedFreqs $ era
		u = topNPatterns n $ getUntied $ getCombinedTiedUntiedFreqs $ era

-- split rags into n subrags and keep them apart
splitRags :: Int -> Rags -> [Rags]
splitRags n rs = transpose $ map (splitRag n) rs

-- split a rag into n subrags for temporal analysis
splitRag :: Int -> Rag -> Rags
splitRag n (Rag rtc i r m) = map joinRag $ rm where 
	joinRag (a,b) = (Rag rtc i a b)
	rm = zip (binList n r) (binList n m)
