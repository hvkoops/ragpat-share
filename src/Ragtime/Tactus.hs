module Tactus where

import Ragtime.LongHigLee
import Ragtime.Pattern 

threshold = 0.85

findTactus :: QMidiScore -> Int
findTactus ac | (thrd/frst) >  threshold = 2
              | (thrd/frst) <= threshold = 4 where
    frst = sum $ map (!!0) $ map ptrnToInt $ toPatternsAccomp ac
	thrd = sum $ map (!!2) $ map ptrnToInt $ toPatternsAccomp ac