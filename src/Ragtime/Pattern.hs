{-# OPTIONS_GHC -Wall                #-}
{-# OPTIONS_GHC -XBangPatterns #-}
module Ragtime.Pattern  where

import ZMidi.Score
import ZMidi.IO.Common            ( readQMidiScore, readQMidiScoreSafe, 
                                    warning )
import ZMidi.Skyline.MelFind      ( findMelodyQuant, getAccompQuant )
import ZMidi.IMA.GTInfo 
import Ragtime.Compendium.Parser  ( RTC (..), getRTCMeta, approxYear, 
                                    preciseYear, preciseYearInt, 
                                    rtcidInt, approxYearInt )

import Data.List                  ( intercalate )
import Control.Arrow              ( first, second )
import Ragtime.Evaluation

import Data.List.Split
import Data.List
import Ragtime.LongHigLee 
import Ragtime.Types
import Control.Monad
import System.FilePath.Posix
import Ragtime.PatternNgrams

--------------------------------------------------------------------------------
-- Converting to patterns
--------------------------------------------------------------------------------

--TODO re-examine this pattern code: QBins <-> GridUnit confusion etc.

-- | Takes a midiscore, quantises it, finds the melody, and turns it into a 
-- 'Pattern' list, where every 'Pattern' represents a beat
toPatterns :: QMidiScore -> [Pattern]
toPatterns ms = scoreToPatterns (Time . tpb $ getMinGridSize ms) (qToQBins ms) 
              . findMelodyQuant $ ms where
                
  scoreToPatterns :: Time -> QBins -> Voice -> [Pattern]
  scoreToPatterns ml qb = groupEvery qb . toPat [0, ml .. ] . map onset where
  
    -- from onset Time to Onset type
    toPat :: [Time] -> [Time] -> [Onset]
    toPat [] []  = []
    toPat [] _   = error "no more grid?" -- impossible?
    toPat _  []  = []
    toPat (g:gs) (d:ds) | g == d = I : toPat gs ds
                        | g <  d = if d - g < ml
                                   then error "unquantised interval encountered"
                                   else O : toPat gs (d:ds)
                                 
  -- | Groups a list of patterns in fixed size lists, if the last list is not 
  -- of the same length, the remainder is filled with 'X's
  groupEvery :: QBins -> Pattern -> [Pattern]
  groupEvery (QBins x) p | glen == x =  g : groupEvery (QBins x) r
                         | otherwise = [g ++ replicate (x - glen) X]
    where (g,r) = splitAt x p 
          glen  = length g

-- | Return pitches in bar. No rhythm
toMelody :: QMidiScore -> [Pattern] -> [RagBarM]
toMelody ms p = (matchPat p) $ map (pitch.getEvent) $ findMelodyQuant ms where
  -- We use (-1,C) as a filler note (silence)
  matchPat :: [Pattern] -> [Pitch] -> [RagBarM] 
  matchPat p n = liftM RagBarM $ ((matchPat' n ss)++[[]]) where  
    ss = map sum $ (map.map) onsetToInt $ p 
    matchPat' :: [Pitch] -> [Int] -> [[Pitch]] 
    matchPat' [] ns     = []
    matchPat' p  (n:ns) = (take n p):(matchPat' (drop n p) ns)

-- | Takes a midiscore, quantises it, finds the accompaniment, and turns it into a 
-- 'Pattern' list, where every 'Pattern' represents a beat
toPatternsAccomp :: QMidiScore -> [Pattern]
toPatternsAccomp ms = scoreToPatterns (Time . tpb $ getMinGridSize ms) (qToQBins ms) 
                      . getAccompQuant $ ms where
                
  scoreToPatterns :: Time -> QBins -> Voice -> [Pattern]
  scoreToPatterns ml qb = groupEvery qb . toPat [0, ml .. ] . rmdups . map onset where
  
    -- from onset Time to Onset type
    toPat :: [Time] -> [Time] -> [Onset]
    toPat [] []  = []
    toPat [] _   = error "no more grid?" -- impossible?
    toPat _  []  = []
    toPat (g:gs) (d:ds) | g == d = I : toPat gs ds
                        | g <  d = if d - g < ml
                                   then error "unquantised interval encountered"
                                   else O : toPat gs (d:ds)
                                 
  -- | Groups a list of patterns in fixed size lists, if the last list is not 
  -- of the same length, the remainder is filled with 'X's
  groupEvery :: QBins -> Pattern -> [Pattern]
  groupEvery (QBins x) p | glen == x =  g : groupEvery (QBins x) r
                         | otherwise = [g ++ replicate (x - glen) X]
    where (g,r) = splitAt x p 
          glen  = length g

-- | Because the tied pattern crosses a barline, we take overlapping segments
-- of two bars. Also, because the prototypical patterns are at the eighth note 
-- level in a 2/4 and at the quarter note level in the 4/4 and 2/2 the segment
-- boundaries are determined by the 'TimeSig'nature.
createOverlapSegs :: TimeSig -> [Pattern] -> [Pattern]
createOverlapSegs ts p = case ts of
  (TimeSig 2 2 _ _ ) -> takeConcatOverlap 8 p
  (TimeSig 2 4 _ _ ) -> takeConcatOverlap 4 p
  (TimeSig 4 4 _ _ ) -> takeConcatOverlap 8 p
  _                  -> error "reGroup: invalid time signature"
  
takeConcatOverlap :: Int -> [[a]] -> [[a]]
takeConcatOverlap _ [] = []
takeConcatOverlap i l  = let (top, rest) = splitAt i l 
                         in concat top 
                          : takeConcatOverlap i (drop (i `div` 2) top ++ rest)

-- remove duplicates from list
rmdups :: (Ord a) => [a] -> [a]
rmdups = map head . group . sort

---- from onset Time to Onset type
--toPat :: [Time] -> [Time] -> Time -> [Onset]
--toPat [] [] ml = []
--toPat [] _  ml = error "no more grid?" -- impossible?
--toPat _  [] ml = []
--toPat (g:gs) (d:ds) ml | g == d = I : toPat gs ds ml
--                       | g <  d = if d - g < ml
--                                  then error "unquantised interval encountered"
--                                  else O : toPat gs (d:ds) ml
     
showPats :: [Pattern] -> String
showPats = intercalate "\n" . map show

--------------------------------------------------------------------------------
-- Important tests for valid midi files
--------------------------------------------------------------------------------

-- | Returns the percentage of onsets that are not on grid positions 0, 3, 6,
-- and 9 (of 12) for a 'Pattern' (by applying 'percTripGridOnsets)
getPercTripGridOnsets :: QMidiScore -> Double
getPercTripGridOnsets = percTripGridOnsets . toPatterns 

--------------------------------------------------------------------------------
-- Matching beat subdivisions
--------------------------------------------------------------------------------

-- | Returns the percentage of onsets that are not on grid positions 0, 3, 6,
-- and 9 (of 12) for a 'Pattern'
percTripGridOnsets :: [Pattern] -> Double
percTripGridOnsets ps = 
  let (trip,rest) = foldr step (0,0) ps

      step :: Pattern -> (Int, Int) -> (Int, Int)
      step p r = r `seq` foldr doPat r . zipWith (,) [0..11] $ p where 
        
        -- increase the first when off the straight grid
        doPat :: (Int, Onset) -> (Int, Int) -> (Int, Int)
        doPat (1 , I) x = first  succ x
        doPat (2 , I) x = first  succ x
        doPat (4 , I) x = first  succ x
        doPat (5 , I) x = first  succ x
        doPat (7 , I) x = first  succ x
        doPat (8 , I) x = first  succ x
        doPat (10, I) x = first  succ x
        doPat (11, I) x = first  succ x
        doPat _       x = second succ x
        
  in fromIntegral trip / fromIntegral (trip + rest)

--------------------------------------------------------------------------------
-- Matching rhythmic patterns
--------------------------------------------------------------------------------

-- | normalises a match by dividing the matches by the total of matched bars
matchRatio :: (Int, Int) -> Double
matchRatio (mch,mis) = fromIntegral mch / fromIntegral (mch + mis)

matchPrint :: (Int, Int) -> (Int, Int) -> [String]
matchPrint (m1,ms1) (m2,_ms2) = [ show (fromIntegral (m1 + m2)
                                /       fromIntegral (m1 + ms1) :: Double)
                                , show m1, show m2, show (m1 + ms1) ]

-- | Calculates the matches and mismatches, respectively
matchPat :: [Pattern] -> Pattern -> (Int, Int)
matchPat ps p = foldr step (0,0) ps where
  
  step :: Pattern -> (Int,Int) -> (Int, Int)
  step x r | perfect p x = first  succ r -- if perfect increase the first,
           | otherwise   = second succ r -- if mismatch the second

--data Onset = X -- | don't care symbol, ignored in all evaluation
--           | O -- | No onset
--           | I -- | Onset
--             deriving Eq
             
--instance Show Onset where
--  show o = case o of
--            X -> "*"
--            O -> "_"
--            I -> "x"
--  showList l s = s ++ (intercalate " " . map show $ l)

--instance Show Onset where
--  show o = case o of
--            X -> "X"
--            O -> "O"
--            I -> "I"
--  --showList l s = s ++ (intercalate " " . map show $ l)

--instance Matchable Onset where
--  -- eqi :: Onset -> Onset -> EqIgnore
--  eqi I I = Equal
--  eqi O O = Equal
--  eqi X _ = Ignore
--  eqi _ X = Ignore
--  eqi _ _ = NotEq  
  
--type Pattern = [Onset]

untied1, untied2, tied1, tied2 :: Pattern

untied1 = [ I, I, O, I,  X, X, X, X,  X, X, X, X,  X, X, X, X ]
untied2 = [ X, X, X, X,  I, I, O, I,  X, X, X, X,  X, X, X, X ]

tied1   = [ X, X, I, I,  O, I, X, X,  X, X, X, X,  X, X, X, X ]
tied2   = [ X, X, X, X,  X, X, I, I,  O, I, X, X,  X, X, X, X ]

tied2'  = [ O, I, X, X,  X, X, I, I]

upScalePat :: TimeSig -> Pattern -> Pattern
upScalePat ts p = case ts of 
  (TimeSig 2 2 _ _ ) -> scale 5 p
  (TimeSig 2 4 _ _ ) -> scale 2 p
  (TimeSig 4 4 _ _ ) -> scale 5 p
  _                  -> error "upScalePat: invalid time signature"
  

-- -- rescale bar to double length
-- downsampleTactus :: Int -> [Pattern] -> [Pattern]
-- downsampleTactus 4 l = l
-- downsampleTactus 2 l = downsampleDoubleR l where
--   downsampleDoubleR :: [Pattern] -> [Pattern]
--   downsampleDoubleR []    = []
--   downsampleDoubleR (p:ps) = (intercalateSilence p)):(downsampleDoubleR ps)

scale :: Int -> Pattern -> Pattern
scale _    []     = []
scale fact (X:tl) = X : replicate fact X ++ scale fact tl
scale fact (h:tl) = h : replicate fact O ++ scale fact tl

--------------------------------------------------------------------------------
-- Tactus finding
--------------------------------------------------------------------------------
tactusThrshld :: Fractional a => a
tactusThrshld = 0.80

-- fix: RagPat: src/Ragtime/Pattern.hs:(284,1)-(287,79): Non-exhaustive patterns in function findTactus
findTactus :: QMidiScore -> Int
findTactus ms | (frst == 0)                      = 0 -- weird but happens??
              | ((/) thrd frst) >  tactusThrshld = 2
              | ((/) thrd frst) <= tactusThrshld = 4 where
    frst = fromIntegral $ sum $ map (!!0) $ map ptrnToInt $ toPatternsAccomp ms
    thrd = fromIntegral $ sum $ map (!!2) $ map ptrnToInt $ toPatternsAccomp ms

findTactus' :: TimeSig  -> QMidiScore -> (Int, TimeSig)
findTactus' i ms | (frst == 0)                      = (0, i) -- weird but happens??
                 | ((/) thrd frst) >  tactusThrshld = (2, i)
                 | ((/) thrd frst) <= tactusThrshld = (4, i) where
    frst = fromIntegral $ sum $ map (!!0) $ map ptrnToInt $ toPatternsAccomp ms
    thrd = fromIntegral $ sum $ map (!!2) $ map ptrnToInt $ toPatternsAccomp ms    