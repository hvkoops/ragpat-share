{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Ragtime.Types where

import ZMidi.Score
import ZMidi.IO.Common            ( readQMidiScore )
import ZMidi.Skyline.MelFind      ( findMelodyQuant )
import Ragtime.Compendium.Parser  ( RTC (..), getRTCMeta, approxYear )

import Data.List                  ( intercalate )
import Control.Arrow              ( first, second )
import Ragtime.Evaluation
import Ragtime.Compendium.Parser

import Data.List.Split
import Data.Binary
import GHC.Generics (Generic)

data Onset = X -- | don't care symbol, ignored in all evaluation
           | O -- | No onset
           | I -- | Onset
             deriving (Eq, Generic)
instance Binary Onset             

instance Show Onset where
  show o = case o of
            X -> "X"
            O -> "O"
            I -> "I"
  --showList l s = s ++ (intercalate " " . map show $ l)

instance Matchable Onset where
  -- eqi :: Onset -> Onset -> EqIgnore
  eqi I I = Equal
  eqi O O = Equal
  eqi X _ = Ignore
  eqi _ X = Ignore
  eqi _ _ = NotEq  

newtype Syncopation    = Syncopation    { sync    :: Int } 
                          deriving ( Eq, Num, Ord, Enum, Real, Integral, Generic )
instance Binary Syncopation

newtype LHLVal            = LHLVal { lhlTreeVal :: Int } 
                          deriving ( Eq, Num, Ord, Enum, Real, Integral )
type LHLTree              = [LHLVal]

instance Show LHLVal where
  show o =  show $ lhlTreeVal o
instance Show Syncopation where
  show o =  show $ sync o

type Pattern    = [Onset]
type BarPattern = [Onset]
type LHLPattern = [(Syncopation, BarPattern)]
type Tactus = Int
type Depth = Int

type Era        = (Year, Year)
type Year       = Int
type Period     = Int

type BinOnsets          = [Int]
type TiedPatterns       = BinOnsets
type UntiedPatterns     = BinOnsets
type TiedPatternsBoth   = (TiedPatterns,TiedPatterns)
type UntiedPatternsBoth = (UntiedPatterns,UntiedPatterns)

--types that once were the csv contents
-- This might be slow for big data, Map?
data Rag = Rag {  rtc         :: RTC
                 ,rtcmidi     :: RTCMidi
                 ,rpatterns   :: [RagBarR]
                 ,mpatterns   :: [RagBarM]
               } deriving ( Generic )
instance Binary Rag

instance Show Rag where
  show (Rag r rm p m) =    (show r) ++ "\n" 
                        ++ (show rm) ++ "\n"
                        ++ intercalate "\n" (map show $ p) ++ "\n"
                        ++ intercalate "\n" (map show $ m)
type Rags = [Rag]


data RagBarR = RagBarR { barsync      :: Syncopation
                        ,onsetsync    :: [Syncopation]
                        ,pattern      :: BarPattern
                       } deriving (Show, Generic, Eq)
instance Binary RagBarR
-- instance Show RagBarR where

--maybe add the melody as well? 
data RagBarM = RagBarM  { mel     :: [Pitch]
                        } deriving ( Show, Generic )
instance Binary RagBarM

-- pretty print the rythmical patterns of a Rag
pprintRagBarR :: RagBarR -> String
pprintRagBarR b = (show $ barsync b)++": "++(show $ pattern b)++"\n"

-- from Matchfile.hs:
-- | Representing a Midi filepath 
data RTCMidi = RTCMidi { baseDir      :: FilePath
                       , folder       :: RTCFolder 
                       , fileName     :: String
                       -- some properties of the midifile 
                       -- (We don't store the complete midi files to save space)
                       , nrOfVoices   :: Int
                       , rtcTimeSig   :: [Timed TimeSig]
                       , tripletPerc  :: Double
                       , avgDeviation :: Float
                       , rtcGridUnit  :: GridUnit
                       } deriving (Show, Eq, Generic)
instance Binary RTCMidi                       