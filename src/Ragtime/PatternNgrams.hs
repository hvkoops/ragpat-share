module Ragtime.PatternNgrams where

import Data.List
import qualified Data.Sequence as Sequence
import Data.Foldable (toList)
import Ragtime.Statistics
import Ragtime.Types
import Ragtime.LongHigLee
import Data.Ord

-- import Diagrams.Prelude
-- import Diagrams.Backend.SVG
-- import Data.Default
-- import Graphics.Rendering.HPlot
-- import System.Random
-- import Data.List.Split
-- import Control.Lens ((^.))

-- xs :: [[Double]]
-- xs = chunksOf 30.take 900.randomRs (-100, 100).mkStdGen $ 2

-- labels :: [String]
-- labels = chunksOf 5.take 150.randomRs ('a', 'z').mkStdGen $ 22

-- pltRN = do
--     let xaxis = indexAxis 30 labels 0.09
--                 $ with & tickLen .~ (-0.05)
--                        & labelOpts .~ (with & labelRotation .~ (1/4))
--         yaxis = indexAxis 30 labels 0.09 $ with & tickLen .~ (-0.05)
--         area = plotArea 5.5 4.8 (yaxis, emptyAxis, emptyAxis, xaxis)
--         heat = heatmap xs def
--         legend = colorKey 0.3 4.8 (minimum.concat $ xs, maximum.concat $ xs) (def^.palette)
--         p = area <+ (heat, BL)
--     renderSVG "1.svg" (Dims 480 480) $ showPlot p ||| strutX 0.2 ||| alignB legend

-- Calculate nGram frequencies on Rag rhythmic patterns
-- TODO: generalize 16
-- ragGrams :: Int -> Rag -> [(Double, [Int])]
-- ragGrams n r = nGramFreqs (nub $ nGrams n bps) (nGrams n bps) where
-- 	bps = map binToDec.(map.map) onsetToInt . 
-- 			map (take 16) . map pattern.rpatterns $ r

ragGramT :: Int -> Rag -> [Double]
ragGramT n r = fst.unzip $ nGramFreqs (nGrams n t) (nGrams n t) where
				t = pattList $ r

pattList :: Rag -> [Int]
pattList r = map binToDec . concat . map (\(a,b) -> [a,b]).(map getTiedPat). 
				(map.map) onsetToInt.map pattern.rpatterns $ r

-- return the frequencies of tied ngrams
tiedGrams :: Int -> Rag -> [(Double, [Int])]
tiedGrams n r = nGramFreqs (nub $ nGrams n t) (nGrams n t) where
	t = map binToDec . concat . map (\(a,b) -> [a,b]).(map getTiedPat).
			(map.map) onsetToInt.map pattern.rpatterns $ r

-- return the frequencies of untied ngrams
untiedGrams :: Int -> Rag -> [(Double, [Int])]
untiedGrams n r = nGramFreqs (nub $ nGrams n t) (nGrams n t) where
	t = map binToDec . concat . map (\(a,b) -> [a,b]).(map getUntiedPat).
			(map.map) onsetToInt.map pattern.rpatterns $ r

-- TODO: generalize 8
ppNgrams :: [(Double, [Int])] -> IO ()
ppNgrams p =  putStrLn . intercalate "\n" $ map show . map g . 
				reverse . sortBy (comparing fst) $ p where 
					g (n,l) = (n, map (decToBin 8) $ l)

-- get the frequencies of the source compared to the targer
nGramFreqsNorm :: [[Int]] -> [[Int]] -> [(Double, [Int])]
nGramFreqsNorm [] _ 	= []
nGramFreqsNorm (s:ss) t = (nGramFreqsNorm' s t):(nGramFreqsNorm ss t) where 
	nGramFreqsNorm' :: [Int] -> [[Int]] -> (Double, [Int])
	nGramFreqsNorm' p a = ((/) 	(fromIntegral $ length.filter (== p) $ a) 
							(fromIntegral $ length a), p)

nGramFreqs :: [[Int]] -> [[Int]] -> [(Double, [Int])]
nGramFreqs [] _ 	= []
nGramFreqs (s:ss) t = (nGramFreqs' s t):(nGramFreqs ss t) where 
	nGramFreqs' :: [Int] -> [[Int]] -> (Double, [Int])
	nGramFreqs' p a = ((fromIntegral $ length.filter (== p) $ a), p)

nGrams :: Int -> [a] -> [[a]]
nGrams n l = take (length l - (n - 1)) . map (take n) . tails $ l

combosOfLength :: Int -> [a] -> [[a]]
combosOfLength n = (map toList).(Sequence.replicateM n)
