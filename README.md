### Studying Rythmical Patterns in Ragtimes.

This repository contains the code for the paper [Koops, Hendrik Vincent and
Volk, Anja, and W. Bas de Haas. "Corpus-Based Rhythmic Pattern Analysis of
Ragtime Syncopation." (ISMIR 2015)](
http://dspace.library.uu.nl/bitstream/handle/1874/326596/ISMIR2015.pdf). It
presents a corpus-based study on rhythmic patterns in the RAG-collection of
approximately 11.000 symbolically encoded ragtime pieces. While characteristic
musical features that define ragtime as a genre have been debated since its
inception, musicologists argue that specific syncopation patterns are most
typical for this genre. Therefore, we investigate the use of syncopation
patterns in the RAG-collection from its beginnings until the present time in
this paper. Using computational methods, this paper provides an overview on
the use of rhythmical patterns of the ragtime genre, thereby offering valuable
new insights that complement musicological hypotheses about this genre.
Specifically, we measure the amount of syncopation for each bar using
Longuet-Higgins and Lee’s model of syncopation, determine the most frequent
rhythmic patterns, and discuss the role of a specific short-long-short (121 or
10100010) syncopation pattern that musicologists argue is characteristic for
ragtime.

![alt tag](./assets/fig1.png)

![alt tag](./assets/fig2.png)

A comparison between the ragtime (pre-1920) and modern (post-1920) era shows
that the two eras differ in syncopation pattern use. 

![alt tag](./assets/fig4.png)

Onset density and amount of syncopation increase after 1920. Moreover, our
study confirms the musicological hypothesis on the important role of the
short-long-short syncopation pattern in ragtime. These findings are pivotal in
developing ragtime genre-specific features.

<!-- ![alt tag](./assets/fig3.png) -->

![alt tag](./assets/fig5.png)

![alt tag](./assets/fig6.png)

![alt tag](./assets/fig7.png)

This package contains two executables: `ragpat` and `melfind`.  

## Executable RagPat
The executable `ragpat` processes the large corpus of ragtime MIDI files.

```
usage: ragpat
```

 
## Executable MelFind
the executable `melfind` is created for individually testing the melody finding algorithm 
described in (Volk & De Haas, 2013)

```
usage: melfind
```


## References
[Volk, Anja, and W. Bas de Haas. "A corpus-based study on ragtime syncopation." 
(2013) In _Proceedings of the International Society for Music Information 
Retrieval Conference_](
http://dspace.library.uu.nl/bitstream/handle/1874/289635/Volk_Haas-ISMIR2013-Ragt
ime.pdf)

[Koops, Hendrik Vincent and Volk, Anja, and W. Bas de Haas. "Corpus-Based Rhythmic Pattern Analysis of Ragtime Syncopation." 
(2015) In _Proceedings of the International Society for Music Information 
Retrieval Conference_](
http://dspace.library.uu.nl/bitstream/handle/1874/326596/ISMIR2015.pdf)